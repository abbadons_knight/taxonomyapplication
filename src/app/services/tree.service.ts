import {TreeModel} from '../models/tree.model';
import {Injectable} from '@angular/core';
import {NodeModel} from '../models/node.model';

@Injectable()
export class TreeService {

  private _tree: TreeModel;

  get tree(): TreeModel {
    return this._tree;
  }

  set tree(value: TreeModel) {
    this._tree = value;
  }

  constructor () {
    this.tree = new TreeModel();
  }

  setActiveNode(setNode: NodeModel) {
    if (this._tree.state.activeNode === setNode) {
      this._tree.state.activeNode.isActive = false;
      this._tree.state.activeNode = null;
    } else {
      if (this._tree.state.activeNode) {
        this._tree.state.activeNode.isActive = false;
      }
      this._tree.state.activeNode = setNode;
      this._tree.state.activeNode.isActive = true;
    }
  }

  getActiveNode() {
    return this._tree.state.activeNode;
  }


}
