export class NodeModel {

  private _id: number;
  private _isCollapsed = false;
  private _isActive = false;
  private _name: string;
  private _description: string;
  private _children: NodeModel[];
  private _isCategory: boolean;
  private _numCategories: number;
  private _numItems: number;
  private _parent: NodeModel;

  get isActive(): boolean {
    return this._isActive;
  }

  set isActive(value: boolean) {
    this._isActive = value;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get isCollapsed(): boolean {
    return this._isCollapsed;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get children(): NodeModel[] {
    return this._children;
  }

  pushToChildren (value: NodeModel) {
    this.children.push(value);
  }

  get isCategory(): boolean {
    return this._isCategory;
  }

  set isCategory(value: boolean) {
    this._isCategory = value;
  }

  get numCategories(): number {
    return this._numCategories;
  }

  set numCategories(value: number) {
    this._numCategories = value;
  }

  get numItems(): number {
    return this._numItems;
  }

  set numItems(value: number) {
    this._numItems = value;
  }

  get parent(): NodeModel {
    return this._parent;
  }

  set parent(value: NodeModel) {
    this._parent = value;
  }

  constructor(id: number, name: string, description: string, isCategory: boolean, parent: NodeModel) {
    this._id = id;
    this._name = name;
    this._description = description;
    this._isCategory = isCategory;
    this._children = <NodeModel[]>[];
    this._parent = parent;
    this._numCategories = 0;
    this._numItems = 0;
  }

  removeFromId(id: number) {
    this.children.splice(this.children.findIndex(value => (value.id === id)), 1);
  }

  public toggleCollapsed() {
    this._isCollapsed = !this._isCollapsed;
  }
}
