import { Component, OnInit } from '@angular/core';
import {TreeService} from '../services/tree.service';

@Component({
  selector: 'app-formpage',
  templateUrl: './formpage.component.html',
  styleUrls: ['./formpage.component.css']
})
export class FormpageComponent implements OnInit {

  protected activatedNode = this.treeService.getActiveNode();
  protected name: string;
  protected desc: string;
  protected selectedOption: string;
  protected typeOptions = ['Category', 'Item'];

  constructor(protected treeService: TreeService) { }

  ngOnInit() {
  }

  onTextEdited() {

  }

  onAddRootClick() {

  }

  onAddClick() {

  }

  onDelClick() {

  }

  onUpdClick() {

  }
}
