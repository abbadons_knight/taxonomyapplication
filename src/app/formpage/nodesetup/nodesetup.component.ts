import {Component, Input, OnInit} from '@angular/core';
import {NodeModel} from '../../models/node.model';

@Component({
  selector: 'app-nodesetup',
  templateUrl: './nodesetup.component.html',
  styleUrls: ['./nodesetup.component.css']
})
export class NodesetupComponent implements OnInit {

  @Input() node: NodeModel;
  constructor() { }

  ngOnInit() {
  }

}
