import {StateModel} from './state.model';
import {NodeModel} from './node.model';

export class TreeModel {

  private _nodes: NodeModel[];
  private _id: number;
  private _state: StateModel;
  private _numCategories = 0;
  private _numItems = 0;

  get nodes(): NodeModel[] {
    return this._nodes;
  }

  get id(): number {
    return this._id;
  }

  get state(): StateModel {
    return this._state;
  }

  constructor() {
    this._nodes = <NodeModel[]>[];
    this._state = new StateModel();
    this._id = 0;
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
    this._nodes.push(new NodeModel(this._id++, 'ahvbauadkkbwniaasdjnwioadhia', 'Desc', true, null));
  }

  public addNode(name: string, description: string, isCategory: boolean): boolean {
    if (!this.state.activeNode) {
      this._nodes.push(new NodeModel(this._id++, name, description, isCategory, null));
      this.adjustCategoryCountOnAdd(isCategory);
      return true;
    } else {
      if (this.state.activeNode.isCategory) {
        this.state.activeNode.pushToChildren(new NodeModel(this._id++, name, description, isCategory, this._state.activeNode));
        this.adjustCategoryCountOnAdd(isCategory);
        return true;
      } else {
        return false;
      }
    }
  }

  public removeNode(): void {
    if (this.state.activeNode.parent == null) {
      const fetchedNode = this._nodes.findIndex(value => (value.id === this.state.activeNode.id));
      this.adjustCategoryCountOnRemove();
      this._nodes.splice(fetchedNode, 1);
    } else {
      this.state.activeNode.parent.removeFromId(this.state.activeNode.id);
    }
    this.state.activeNode = null;
  }

  public updateNode(name: string, description: string, isCategory: boolean) {
    this._state.activeNode.name = name;
    this._state.activeNode.description = description;
    this._state.activeNode.isCategory = isCategory;
  }

  private adjustCategoryCountOnAdd(isCategory: boolean) {
    let currentNode = this._state.activeNode;
    while (currentNode) {
      if (currentNode.parent) {
        if (isCategory) {
          currentNode.numCategories++;
        } else {
          currentNode.numItems++;
        }
      } else {
        if (isCategory) {
          this._numCategories++;
        } else {
          this._numItems++;
        }
      }
      currentNode = currentNode.parent;
    }
  }

  private adjustCategoryCountOnRemove() {
    const numIts = (!this._state.activeNode.isCategory) ? 1 : this._state.activeNode.numItems;
    const numCats = (this._state.activeNode.isCategory) ? (this._state.activeNode.numCategories + 1) : this._state.activeNode.numCategories;

    let currentNode = this._state.activeNode;
    if (currentNode.parent) {
      currentNode.parent.numCategories -= currentNode.numCategories;
      currentNode.parent.numItems -= currentNode.numItems;
      while (currentNode) {
        if (currentNode.parent) {
          currentNode.parent.numItems -= numIts;
          currentNode.parent.numCategories -= numCats;
        } else {
          this._numItems--;
          this._numCategories--;
        }
        currentNode = currentNode.parent;
      }
    } else {
      this._numItems -= (this._state.activeNode.numItems + 1);
      this._numCategories -= (this._state.activeNode.numCategories + 1);
    }
  }
}
