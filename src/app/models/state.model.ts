import {NodeModel} from './node.model';

export class StateModel {

  constructor() {
  }
  private _activeNode: NodeModel;

  get activeNode(): NodeModel {
    return this._activeNode;
  }

  set activeNode(value: NodeModel) {
    this._activeNode = value;
  }
}
