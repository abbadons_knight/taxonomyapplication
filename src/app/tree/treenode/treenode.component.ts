import {Component, Input, OnInit} from '@angular/core';
import {TreeService} from '../../services/tree.service';
import {NodeModel} from '../../models/node.model';

@Component({
  selector: 'app-treenode',
  templateUrl: './treenode.component.html',
  styleUrls: ['./treenode.component.css']
})
export class TreenodeComponent implements OnInit {

  @Input() protected node: NodeModel;

  constructor(public treeService: TreeService) { }

  ngOnInit() {
  }

  toggleActiveNode(event) {
    this.treeService.setActiveNode(this.node);
    if (this.node === this.treeService.getActiveNode()) {
     console.log(this.node);
    }
  }
}
