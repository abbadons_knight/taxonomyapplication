import { Component, OnInit } from '@angular/core';
import {TreeModel} from '../models/tree.model';
import {TreeService} from '../services/tree.service';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent implements OnInit {

  protected tree: TreeModel;

  constructor(protected treeService: TreeService) { }

  ngOnInit() {
    this.tree = this.treeService.tree;
  }

  addOnClick($event) {
    this.treeService.tree.addNode('b', 'b', true);
  }

  removeOnClick($event) {
    this.treeService.tree.removeNode();
  }

  addItemOnClick($event) {
    this.treeService.tree.addNode('b', 'b', false);
  }
}
