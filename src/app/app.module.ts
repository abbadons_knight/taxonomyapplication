import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TreeComponent } from './tree/tree.component';
import {TreeService} from './services/tree.service';
import { TreenodeComponent } from './tree/treenode/treenode.component';
import { HeaderComponent } from './header/header.component';
import { FormpageComponent } from './formpage/formpage.component';
import { ComponentmanagerComponent } from './componentmanager/componentmanager.component';
import { NodesetupComponent } from './formpage/nodesetup/nodesetup.component';

@NgModule({
  declarations: [
    AppComponent,
    TreeComponent,
    TreenodeComponent,
    HeaderComponent,
    FormpageComponent,
    ComponentmanagerComponent,
    NodesetupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [TreeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
